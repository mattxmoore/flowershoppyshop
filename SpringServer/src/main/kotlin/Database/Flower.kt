package Database

data class Flower(
    val id: Int,
    val name: String,
    val amount: Int
)
