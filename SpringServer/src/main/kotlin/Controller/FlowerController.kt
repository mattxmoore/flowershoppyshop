package Controller

import Database.Flower
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class FlowerController {
    @GetMapping("/AllInfo")
    fun index() = listOf(
        Flower(1,"Tulip", 5),
        Flower(2,"Otherflower", 6),
        Flower(3,"AnotherFlower", 33)
    )

}